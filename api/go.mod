module filiparag.com/ftn-raspored/api/v2

go 1.17

require (
	github.com/filiparag/ftn-raspored v0.1.6
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.11
)
